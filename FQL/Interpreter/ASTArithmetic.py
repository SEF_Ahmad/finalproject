from AST import AST

ROOT, LEFT, RIGHT = 0, 1, 2

class ASTArithmetic(AST):

    DataType = None

    def __init__(self, ExpectedType):
        self.DataType = ExpectedType
        super().__init__("Arithmetic", 100)
