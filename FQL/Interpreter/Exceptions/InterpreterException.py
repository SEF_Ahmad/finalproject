from FQLException import FQLException

class InterpreterException(FQLException):
    Message = ""

    def __init__(self, Errno, Error, Message):
        self.Message = Message
        super().__init__(Errno, Error, "Interpreter")


    def Raise(self):
        super().Log()
        self.Terminate("Fatal: " + self.Message + "\nErrno:"
                       + str(self.Errno) + "\nError: " + self.Error)
