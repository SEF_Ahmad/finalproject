from FQLException import FQLException

class ASTException(FQLException):
    Message = ""

    def __init__(self, Errno, Error, Source, Message):
        if Message == "":
            self.Message = Error
        else:
            self.Message = Message
        super().__init__(Errno, Error, Source)

    
    def Raise(self): 
        super().Log()
        self.Terminate("Fatal: [" + self.Errno + "]" + self.Message)