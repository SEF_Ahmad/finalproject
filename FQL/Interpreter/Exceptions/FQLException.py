import traceback
import datetime
import sys


class FQLException(Exception):
    Error = ""
    Errno = 0
    Source = ""
    Trace = ""


    def __init__(self, Errno, Error, Source):
        self.Errno = Errno
        self.Error = Error
        self.Source = Source
        self.Trace = traceback.format_exc()
        super().__init__()


    def Log(self):
        with open("error.log", "a") as Logfile:
            Logfile.write("> Error [" + str(self.Errno) + "] From " + self.Source + " at " 
                + str(datetime.datetime.now().__format__("%d-%m-%Y %H:%M:%S"))+ "  " + self.Error + "\n\nTrace: " + self.Trace +"\n\n")


    def Terminate(self, Message):
        print(Message)
        sys.exit(-1)
