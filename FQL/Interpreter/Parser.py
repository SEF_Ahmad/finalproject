import sys
import os
import re
sys.path.append(os.path.abspath(os.path.dirname(__file__) + '/')+"/Exceptions/")
from ParseException import ParseException
from ASTDeclare import ASTDeclare
from ASTStream import ASTStream
from ASTAssign import ASTAssign
from ASTArithmetic import ASTArithmetic
from ASTStatement import ASTStatement
from ASTFunctionCall import ASTFunctionCall
from ASTIf import ASTIf
from ASTElse import ASTElse
from ASTElif import ASTElif
from ASTEnd import ASTEnd

TKN, TYP, LN = 0, 1, 2

class Parser:
    Source = ""
    SourceCount = 0
    POS = 0
    Line = 1

    #Generated Output
    AST = None
    ASTStream = []
    Varaibles = []
    Constants = []


    #Define the language constructs
    Types = ["File", "Path", "Number", "String", "Event", "Const", "Dataset", "Set"]
    Selectors = ["FROM", "INTO"]
    Sources = ["Here", "Disk", "Index"]
    Operators = [":", "/", "+", "=", "-", "*", "%", ">", "<", "<=", ">=", "==",
                 "{", "}", "(", ")", ",", '->']
    StringDelimiter = ["\""]
    Statements = ["SELECT", "DELETE", "UPDATE", "INSERT", "WHERE", "like", "it"]
    Keywords = ["if", "function", "end", "else", "while", "return", "and", "or", "then"]
    Delimiter = [";"]
    White = ["\n", " ", "\t", "\r"]


    def __init__(self, Source):
        self.Source = (Source.strip()).replace("\t", " ")
        self.SourceCount = len(self.Source)
        self.ASTStream = ASTStream()



    def Ahead(self):
        if self.POS+1 >= self.SourceCount:
            return ""
        else:
            return self.Source[self.POS + 1]



    def AheadNonWhite(self):
        if self.POS >= self.SourceCount:
            return ""
        else:
            i = self.POS
            while self.Source[i] == " ":
                i += 1
            return self.Source[i]




    def IsReserved(self, Token):
        if Token in self.Statements or Token in self.Selectors or Token in self.Types or \
           Token in self.Sources or Token in self.Keywords:
            return True
        else:
            return False



    def is_float(self, s):
        try:
            float(s)
            return True
        except ValueError:
            return False

    def getToken(self):
        while self.POS < self.SourceCount:
            ch = self.Source[self.POS]
            if ch.isalpha():
                return self.getWord()
            elif ch.isdigit():
                return self.getDigit()
            elif(ch == " " or ch == "\n"):
                if ch == "\n":
                    self.Line += 1
                self.POS += 1
                continue
            elif ch == "/" and self.Ahead() == "*":
                return self.getComment()
            elif ch == "\"":
                return self.getString()
            else:
                return self.getOperator()
        return "\n"



    def TokenType(self, Token):
        if Token in self.Selectors:
            return "Selector"
        elif Token in self.Statements:
            return "Statement"
        elif Token in self.Keywords:
            return "Keyword"
        elif Token in self.Types:
            return "Type"
        elif Token in self.Operators:
            return "Operator"
        elif Token in self.Delimiter:
            return "Delimiter"
        elif Token in self.Sources:
            return "Source"
        elif Token.isdigit() or self.is_float(Token):
            return "Number"
        elif re.match('^(([a-z]|[A-Z]){1}\w*)|(_\w*)$', Token) is not None:
            return "Identifier"
        elif re.match('^\".*\"$', Token) is not None:
            return "String"
        else:
            return "None"




    def getOperator(self):
        Token = ""
        i = self.POS
        Line = self.Line
        while i < self.SourceCount:
            ch = self.Source[i]
            if(ch.isalpha() is False and ch.isdigit() is False and ch != " " and
               ch != "\n" and ch != "\""):
                Token += ch
                Ahead = self.Ahead()
                if Token+Ahead in self.Operators:
                    i += 2
                    Token += Ahead
                    break
                else:
                    i += 1
                    break
            else:
                break
        self.POS = i
        return [Token, self.TokenType(Token), str(Line)]




    """
    This method returns Keyword or identifier like xyz, xyz1, _xyz
    """
    def getWord(self):
        Token = ""
        i = self.POS
        Line = self.Line
        while i < self.SourceCount:
            ch = self.Source[i]
            if((ch.isalpha() or ch.isdigit()) and ch not in self.White):
                Token += ch
                i += 1
            else:
                break
        self.POS = i
        #Determine the real type of the Word
        return [Token, self.TokenType(Token), str(Line)]




    """
    This method returns digits, and even if they contain chars like 123, 123a, 155x
    """
    def getDigit(self):
        Token = ""
        i = self.POS
        Line = self.Line
        while i < self.SourceCount:
            ch = self.Source[i]
            if((ch.isalpha() or ch.isdigit() or ch == ".") and ch != " " and ch != "\n"):
                Token += ch
                i += 1
            else:
                break
        self.POS = i
        return [Token, self.TokenType(Token), str(Line)]




    """
    This method returns enclosed string, like "string"
    """
    def getString(self):
        Token = "\""
        i = self.POS + 1
        Line = self.Line
        while i < self.SourceCount:
            ch = self.Source[i]
            if(ch != "\"" and ch != "\n"):
                Token += ch
                i += 1
            elif ch == "\"":
                Token += ch
                i += 1
                break
            else:
                break #nothing
        self.POS = i
        return [Token, self.TokenType(Token), str(Line)]




    def getComment(self):
        Token = "/*"
        i = self.POS + 2
        Line = self.Line
        while i < self.SourceCount:
            ch = self.Source[i]
            if(ch != "*" and self.Ahead() != "/"):
                if ch == "\n":
                    self.Line += 1
                Token += ch
                i += 1
            else:
                Token += "*/"
                break
        self.POS = i + 2
        return [Token, "Comment", str(Line)]



    #----------------------------------- Rules -----------------------------------




    def Parse(self):
        Token = []
        while self.POS < self.SourceCount:
            Token = self.getToken()
            if Token[TYP] == "Identifier":
                TokenAhead = self.getToken()
                if TokenAhead[TKN] == "=":
                    self.RuleAssignment(Token[TKN], "=")
                elif TokenAhead[TKN] == "(":
                    self.RuleFunctionCall(Token[TKN], [])
            elif Token[TYP] == "Type":
                self.RuleDeclaration(Token[TKN])
            elif Token[TYP] == "Delimiter":
                continue
            elif Token[TYP] == "Selector":
                self.RuleStatement(Token[TKN])
            elif Token[TKN] == "if":
                self.RuleIf(Token)
            elif Token[TKN] == "else":
                self.RuleIf(Token)
            elif Token[TKN] == "end" and self.AheadNonWhite() == "i":
                self.RuleIf(Token)
            elif Token[TYP] == "Comment" or Token[TKN] in self.White:
                continue
            else:
                pe = ParseException(1290, "Undefined syntax at line "+Token[LN],
                                    "Syntax Error")
                pe.Raise()



    def RuleStatement(self, Keyword):
        Token = self.getToken()
        if Token[TYP] == "Identifier" and self.AheadNonWhite() == ":":
            Selector = Token[TKN]
            Token = self.getToken()
            StatementStream = []
            while Token is not None and Token[TKN] not in self.Delimiter and \
                  Token[TKN] not in self.Selectors and Token[TKN] not in self.Types:
                if Token[TKN] in self.White or Token[TKN] == ":":
                    Token = self.getToken()
                    continue
                StatementStream.append(Token[TKN])
                Token = self.getToken()
            A = ASTStatement("Statement")
            A.Selector = Selector
            A.Root = ":"
            A.Left = Keyword
            A.Right = StatementStream
            A.Line = Token[LN]
            self.ASTStream.Append(A)
            if Token is None or Token[TKN] == "\n" or Token[TYP] != "Delimiter":
                pe = ParseException(1210, "Expected end of statement at line "+Token[LN],
                                    "Syntax Error")
                pe.Raise()
            return ";"





    def RuleFunctionCall(self, FuncName, ArgList):
        Token = self.getToken()
        if Token[TYP] == 'String' or Token[TYP] == 'Number' or Token[TYP] == 'Identifier':
            ArgList.append(Token[TKN])
            if self.AheadNonWhite() in [",", ")"]:
                self.RuleFunctionCall(FuncName, ArgList)
        elif Token[TKN] == ')' and self.AheadNonWhite() in self.Delimiter:
            A = ASTFunctionCall("FunCall")
            A.Root = FuncName
            A.Left = ArgList
            A.Right = None
            A.Line = Token[LN]
            self.ASTStream.Append(A)
            return ";"
        elif Token[TKN] == ',':
            self.RuleFunctionCall(FuncName, ArgList)
        elif self.AheadNonWhite() not in self.Delimiter:
            pe = ParseException(1210, "Expected end of statement at line " + Token[LN],
                                "Syntax Error")
            pe.Raise()
        else:
            pe = ParseException(1245, "Corrupted function call at line " + Token[LN],
                                "Syntax Error")
            pe.Raise()




    def RuleAssignment(self, VarName, Operator):
        Token = self.getToken()
        TknTyp = Token[TYP]
        if TknTyp == "Operator" and Token[TKN] == "{":
            return self.RuleAssignmentObject(VarName, Operator, {})
        elif TknTyp == "Identifier" and self.AheadNonWhite() == "(":
            self.getToken() #pass the '(' character
            return self.RuleAssignmentFunction(VarName, [Token[TKN]])
        elif TknTyp == "Identifier" or TknTyp == "Number" or Token[TKN] == "(":
            return self.RuleAssignmentArithmetic(Token, VarName)
        elif TknTyp == "Source" and (self.AheadNonWhite() == ":" or self.AheadNonWhite() in self.Delimiter):
            return self.RuleAssignmentPath(VarName, [Token[TKN]], False)
        elif TknTyp == "String":
            return self.RuleAssignmentString(VarName, Token)
        else:
            pe = ParseException(1250, "Assignment expects function, arithmetic, or object at line "+Token[LN],
                                "Syntax Error")
            pe.Raise()




    def RuleAssignmentString(self, VarName, Token):
        if Token[TYP] == "String":
            if self.AheadNonWhite() in self.Delimiter:
                A = ASTArithmetic("String")
                A.Root = "="
                A.Left = VarName
                A.Right = Token[TKN]
                A.Line = Token[LN]
                self.ASTStream.Append(A)
                return ";"
            else:
                pe = ParseException(1210, "Expected end of statement at line "+Token[LN],
                                    "Syntax Error")
                pe.Raise()
        else:
            pe = ParseException(1213, "Expected string at line "+Token[LN],
                                "Syntax Error")
            pe.Raise()




    def RuleAssignmentPath(self, VarName, PathElements, IsColon):
        Token = self.getToken()
        if Token[TYP] == "Identifier" and IsColon:
            name = Token[TKN]
            while self.AheadNonWhite() != ":" and self.AheadNonWhite() not in self.Delimiter:
                name += self.getToken()[TKN]
            PathElements.append(name)
            return self.RuleAssignmentPath(VarName, PathElements, False)
        elif Token[TKN] == "{" and IsColon:
            while Token[TKN] != "}" and Token[TKN] not in self.Delimiter:
                PathElements.append(Token[TKN])
                Token = self.getToken()
            if Token[TKN] != "}":
                pe = ParseException(1265, "Expected } at line "+Token[LN],
                                "Syntax Error")
                pe.Raise()
            else:
                PathElements.append(Token[TKN])
                return self.RuleAssignmentPath(VarName, PathElements, False)
        elif IsColon is not True and Token[TKN] == ":":
            return self.RuleAssignmentPath(VarName, PathElements, True)
        elif Token[TKN] in self.Delimiter or Token[TKN] == ",":
            A = ASTAssign("Path")
            A.Root = "="
            A.Left = VarName
            A.Right = PathElements
            A.Line = Token[LN]
            self.ASTStream.Append(A)
            return Token[TKN]
        else:
            pe = ParseException(1260, "Path is corrupted at line "+Token[LN],
                                "Syntax Error")
            pe.Raise()





    def RuleAssignmentArithmetic(self, FirstToken, VarName):
        try:
            Output = self.ToPostfix(FirstToken[TKN], [], [])
            A = ASTArithmetic("Arithmetic")
            A.Root = "="
            A.Left = VarName
            A.Right = Output
            A.Line = FirstToken[LN]
            self.ASTStream.Append(A)
        except Exception:
            pe = ParseException(1240, "Arithmetic expression corrupted at line "+FirstToken[LN],
                                "Syntax Error")
            pe.Raise()
        return ";"




    def RuleIf(self, Keyword):
        if Keyword[TKN] == "if":
            try:
                Condition = self.ToPostLogix([], [])
                A = ASTIf("If")
                A.Root = "if"
                A.Left = Condition
                A.Right = None
                A.Line = Keyword[LN]
                self.ASTStream.Append(A)
            except Exception:
                pe = ParseException(1270, "Logic expression corrupted at line "+Keyword[LN],
                                    "Syntax Error")
                pe.Raise()
        elif Keyword[TKN] == "else":
            Token = self.getToken()
            if Token[TKN] == "if":
                try:
                    Condition = self.ToPostLogix([], [])
                    A = ASTElif("Elif")
                    A.Root = "elif"
                    A.Left = Condition
                    A.Right = None
                    A.Line = Token[LN]
                    self.ASTStream.Append(A)
                except Exception:
                    pe = ParseException(1270, "Logic expression corrupted at line "+Keyword[LN],
                                        "Syntax Error")
                    pe.Raise()
            else:
                if Token[TKN] != ":":
                    pe = ParseException(1275, "expected : at line "+Keyword[LN],
                                        "Syntax Error")
                    pe.Raise()
                else:
                    A = ASTElse("Else")
                    A.Root = "else"
                    A.Left = None
                    A.Right = None
                    A.Line = Token[LN]
                    self.ASTStream.Append(A)
                    return "else"
        elif Keyword[TKN] == "end":
            TokenAhead = self.getToken()
            if TokenAhead[TKN] == "if" and self.AheadNonWhite() in self.Delimiter:
                A = ASTEnd("endif")
                A.Root = "endif"
                A.Left = None
                A.Right = None
                A.Line = TokenAhead[LN]
                self.ASTStream.Append(A)
                return ";"
            pe = ParseException(1275, "expected end if at line "+TokenAhead[LN],
                                    "Syntax Error")
            pe.Raise()




    def ToPostLogix(self, Condition, Stack):
        Token = self.getToken()
        if Token[TKN] == "(":
            Stack.append(Token[TKN])
        elif Token[TYP] == "Identifier" or Token[TYP] == "Number" or Token[TYP] == "String" or\
             (Token[TYP] == "Operator" and Token[TKN] != ")"):
            Condition.append(Token[TKN])
        elif len(Stack) > 0 and Token[TKN] == "and" and Stack[len(Stack) - 1] == "or":
            while len(Stack) > 0 and Stack[len(Stack) - 1] != "(" and Stack[len(Stack) - 1] != ")" and Stack[len(Stack) - 1] != "or":
                Condition.append(Stack.pop())
            Stack.append(Token[TKN])
        elif Token[TKN] == "and" or Token[TKN] == "or":
            Stack.append(Token[TKN])
        elif Token[TKN] == ")":
            while len(Stack) > 0 and ((self.TokenType(Stack[len(Stack) - 1]) in ["Identifier", "Number", "String", "Operator"] and Token[TKN] != ")") or\
                  Stack[len(Stack) - 1] == "or" or Stack[len(Stack) - 1] == "and"):
                Condition.append(Stack.pop())
            if len(Stack) > 0:
                if Stack[len(Stack) - 1] != "(":
                    raise SyntaxError
                else: 
                    Stack.pop()
        elif Token[TKN] == "then":
            if self.getToken()[TKN] != ":":
                pe = ParseException(1275, "expected ':' at line "+Token[LN],
                                    "Syntax Error")
                pe.Raise()
            while len(Stack) > 0:
                Top = Stack[len(Stack) - 1]
                if(Top == "(" or Top == ")"):
                    raise SyntaxError
                Condition.append(Stack.pop())
            return Condition
        elif Token is None or Token[TYP] == "Delimiter":
            raise SyntaxError
        return self.ToPostLogix(Condition, Stack)





    def ToPostfix(self, ch, Output, Stack):
        pre = {"(": 4, ")": 4, "*":3, "/": 3, "+": 2, "-": 2}
        if ch.isdigit() or self.TokenType(ch) == "Identifier":
            Output.append(ch)
        elif ch in ["+", "/", "-", "*"]:
            while len(Stack) > 0 and Stack[len(Stack) - 1] != "(" and Stack[len(Stack) - 1] != ")" and pre[Stack[len(Stack) - 1]] > pre[ch]:
                Output.append(Stack.pop())
            Stack.append(ch)
        elif ch == "(":
            Stack.append(ch)
        elif ch == ")":
            while len(Stack) > 0 and Stack[len(Stack) - 1] in ["+", "/", "-", "*"]:
                Output.append(Stack.pop())
            if len(Stack) > 0 and Stack[len(Stack) - 1] != "(":
                raise SyntaxError
            else:
                Stack.pop()
        elif self.IsReserved(ch):
            raise SyntaxError
        Token = self.getToken()
        if Token is None or Token[TYP] == "Delimiter":
            while len(Stack) > 0:
                Top = Stack[len(Stack) - 1]
                if(Top == "(" or Top == ")"):
                    raise SyntaxError
                Output.append(Stack.pop())
            return Output
        else:
            return self.ToPostfix(Token[TKN], Output, Stack)




    def RuleAssignmentFunction(self, VarName, ArgList):
        Token = self.getToken()
        if Token[TYP] == 'String' or Token[TYP] == 'Number' or Token[TYP] == 'Identifier':
            ArgList.append(Token[TKN])
            if self.AheadNonWhite() in [",", ")"]:
                self.RuleAssignmentFunction(VarName, ArgList)
            else:
                pe = ParseException(1245, "Corrupted function call at line " + Token[LN],
                                    "Syntax Error")
                pe.Raise()
        elif Token[TKN] == ')' and self.AheadNonWhite() in self.Delimiter:
            A = ASTAssign("Function")
            A.Root = "="
            A.Left = VarName
            A.Right = ArgList
            A.Line = Token[LN]
            self.ASTStream.Append(A)
            return ";"
        elif Token[TKN] == ',':
            self.RuleAssignmentFunction(VarName, ArgList)
        elif self.AheadNonWhite() not in self.Delimiter:
            pe = ParseException(1210, "Expected end of statement at line " + Token[LN],
                                "Syntax Error")
            pe.Raise()
        else:
            pe = ParseException(1245, "Corrupted function call at line " + Token[LN],
                                "Syntax Error")
            pe.Raise()





    def RuleAssignmentObject(self, VarName, Operator, Obj):
        Token = self.getToken()
        TknTyp = Token[TYP]
        if TknTyp == "Identifier":
            Key = Token[TKN]
            Token = self.getToken()
            if Token[TKN] == ":":
                Token = self.getToken()
                TknTyp = Token[TYP]
                if TknTyp == "Identifier" or TknTyp == "Number" or TknTyp == "String":
                    Obj[Key] = Token[TKN]
                    Token = self.getToken()
                    if Token[TKN] == ",":
                        return self.RuleAssignmentObject(VarName, Operator, Obj)
                    elif Token[TYP] == "Operator" and Token[TKN] == "}":
                        A = ASTAssign("Object")
                        A.Root = Operator
                        A.Left = VarName
                        A.Right = Obj
                        A.Line = Token[LN]
                        self.ASTStream.Append(A)
                        Token = self.getToken()
                        if Token[TYP] != "Delimiter":
                            pe = ParseException(1210, "Expected end of statement at line " + Token[LN],
                                                "Syntax Error")
                            pe.Raise()
                        else:
                            return ";"
                elif Token[TKN] == "[":
                    ArrayObj = self.RuleReadList(Token)
                    Obj[Key] = ArrayObj
                    if self.AheadNonWhite() == ",":
                        Token = self.getToken()
                        return self.RuleAssignmentObject(VarName, Operator, Obj)
                    elif self.AheadNonWhite() == "}":
                        Token = self.getToken()
                        A = ASTAssign("Object")
                        A.Root = Operator
                        A.Left = VarName
                        A.Right = Obj
                        A.Line = Token[LN]
                        self.ASTStream.Append(A)
                        Token = self.getToken()
                        if Token[TYP] != "Delimiter":
                            pe = ParseException(1210, "Expected end of statement at line " + Token[LN],
                                                "Syntax Error")
                            pe.Raise()
                        else:
                            return ";"

        pe = ParseException(1212, "Expected key:value pair object at line " + Token[LN],
                            "Syntax Error")
        pe.Raise()





    def RuleReadList(self, Token):
        arrayObj = []
        IsComma = False
        while Token[TKN] not in ["\n", "\r", None]:
            Token = self.getToken()
            if Token[TYP] == "Identifier" or Token[TYP] == "Number" or Token[TYP] == "String":
                arrayObj.append(Token[TKN])
                IsComma = False
            elif Token[TKN] == "," and IsComma:
                pe = ParseException(1207, "Expected value not ',' at line " + Token[LN],
                                    "Syntax Error")
                pe.Raise()
            elif Token[TKN] == "," and not IsComma:
                IsComma = True
                continue
            elif Token[TKN] == "]":
                return arrayObj
            else:
                pe = ParseException(1208, "Undefined object assignment at line " + Token[LN],
                                    "Syntax Error")
                pe.Raise()
        pe = ParseException(1208, "Expected ']' at line " + Token[LN],
                            "Syntax Error")
        pe.Raise()





    def RuleDeclaration(self, Type, VarName = ""):
        Token = self.getToken()
        if Token[TYP] == "Identifier":
            A = ASTDeclare(Type)
            A.Root = Type
            A.Left = Token[TKN]
            A.Line = Token[LN]
            self.ASTStream.Append(A)
            return self.RuleDeclaration(Type, A.Left)
        elif Token[TYP] == "Operator" and Token[TKN] == "=": #case of variable init
            if VarName == "":
                pe = ParseException(1203, "Attempt to assign to none variable at line " +Token[LN],
                                    "Syntax Error")
                pe.Raise()
            output = self.RuleAssignment(VarName, Token[TKN])
            if output in self.Delimiter:
                return output
            return self.RuleDeclaration(Type)
        elif Token[TYP] == "Delimiter":
            return ";"
        elif Token[TKN] == ",":
            self.RuleDeclaration(Type)
        else:
            pe = ParseException(1200, "Expected Identifier or '=' at line " + Token[LN],
                                "Syntax Error")
            pe.Raise()
