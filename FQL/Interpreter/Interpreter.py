import sys
import os
import re
import pathlib
import string
import random
import hashlib
import magic
from PIL import Image
import imagehash
sys.path.append(os.path.abspath(os.path.dirname(__file__) + '/')+"/Exceptions/")
from InterpreterException import InterpreterException
from FileObject import FileObject
from SetObject import SetObject
from PathObject import PathObject
from DatasetObject import DatasetObject
from Runtime import Runtime


class FQLInterpreter:
    Source = ""
    Executable = None
    ExecutableSize = 0
    POS = 0
    #Vars
    VarsNumber = {}
    VarsString = {}
    VarsDataset = {}
    VarsPath = {}
    VarsSet = {}
    VarsFile = {}
    VarsConst = {}
    StackIf = [] #To detect what to execute in {if ... else} threads


    def __init__(self, Executable):
        self.Executable = Executable
        self.ExecutableSize = len(Executable)
        self.Runtime = Runtime(self)



    def Execute(self):
        while self.POS < self.ExecutableSize:
            exe = self.Executable[self.POS]
            if exe.Type == "Assign" or exe.Type == "Arithmetic":
                self.ExecuteAssign()
            elif exe.Type == "Declare":
                self.ExecuteDeclare()
            elif exe.Type == "Statement":
                self.ExecuteStatement()
            elif exe.Type == "FunCall":
                self.ExecuteFunCall()
            self.POS += 1



    def Raise(self, Errno, Error, Message="Unfeasible Execution"):
        ie = InterpreterException(Errno, Error + " at line " + str(self.Executable[self.POS].Line), Message)
        ie.Raise()



    def ScopeCheck(self, Var):
        if Var in self.VarsConst or Var in self.VarsString or \
           Var in self.VarsFile or Var in self.VarsDataset or \
           Var in self.VarsPath or Var in self.VarsNumber or  \
           Var in self.VarsSet:
            self.Raise(1400, "Variable is already declared", "Scope Error")



    def TypeCheck(self, Var, Vars):
        if Var not in Vars:
            self.Raise(1405, "Type not compatible", "Type Error")



    def ExecuteDeclare(self):
        Statement = self.Executable[self.POS]
        self.ScopeCheck(Statement.Left)
        if Statement.DataType == "Path":
            self.VarsPath[Statement.Left] = ""
        elif Statement.DataType == "Number":
            self.VarsNumber[Statement.Left] = ""
        elif Statement.DataType == "String":
            self.VarsString[Statement.Left] = ""
        elif Statement.DataType == "File":
            self.VarsFile[Statement.Left] = ""
        elif Statement.DataType == "Set":
            self.VarsSet[Statement.Left] = ""
        elif Statement.DataType == "Const":
            self.VarsConst[Statement.Left] = ""
        elif Statement.DataType == "Dataset":
            self.VarsDataset[Statement.Left] = ""



    def ExecuteFunCall(self):
        Statement = self.Executable[self.POS]
        if hasattr(self.Runtime, Statement.Root):
            fun = getattr(self.Runtime, Statement.Root)
            fun(Statement.Left)
        else:
            self.Raise(1404, "Undefined function", "Runtime Error")



    def ExecuteAssign(self):
        Statement = self.Executable[self.POS]
        if Statement.DataType == "Path":
            self.ExecuteAssignPath()
        elif Statement.DataType in ["String", "Const", "Arithmetic"]:
            if Statement.Left in self.VarsString:
                self.ExecuteAssignString()
            elif Statement.Left in self.VarsNumber:
                self.ExecuteAssignNumber()
            elif Statement.Left in self.VarsConst:
                self.ExecuteAssignConst()
        elif Statement.DataType == "Object":
            self.ExecuteAssignObject()
        elif Statement.DataType == "Function":
            self.ExecuteAssignFunction()




    def ExecuteAssignFunction(self):
        Statement = self.Executable[self.POS]
        if hasattr(self.Runtime, Statement.Right[0]):
            fun = getattr(self.Runtime, Statement.Right[0])
            fun(Statement.Right[1:], Return=Statement.Left)
        else:
            self.Raise(1404, "Undefined function", "Runtime Error")



    def ExecuteAssignString(self):
        Statement = self.Executable[self.POS]
        self.TypeCheck(Statement.Left, self.VarsString)
        self.VarsString[Statement.Left] = Statement.Right[1:-1] #Strip " characters from start & end


    def ExecuteAssignConst(self):
        Statement = self.Executable[self.POS]
        self.TypeCheck(Statement.Left, self.VarsConst)
        self.VarsConst[Statement.Left] = str(Statement.Right).replace('"', '')



    def ExecuteAssignNumber(self):
        Statement = self.Executable[self.POS]
        self.TypeCheck(Statement.Left, self.VarsNumber)
        self.VarsNumber[Statement.Left] = self.EvalPostArithmetic([], Statement.Right, 0)



    def EvalPostArithmetic(self, Stack, Input, Index):
        if Index >= len(Input):
            if len(Stack) > 1:
                self.Raise(1414, "Arithmetic operation corrupted", "Runtime Evaluation")
            return Stack.pop()   #Base case

        if Input[Index] not in ["+", "-", "/", "*"]:
            Stack.append(Input[Index])
        else:
            op2 = int(self.ResolveNumberVariable(Stack.pop()))
            op1 = int(self.ResolveNumberVariable(Stack.pop()))
            if Input[Index] == "*":
                result = op1 * op2
            elif Input[Index] == "/":
                result = op1 / op2
            elif Input[Index] == "+":
                result = op1 + op2
            elif Input[Index] == "-":
                result = op1 - op2
            else:
                self.Raise(1407, "Undefined Arithmetic operator")
            Stack.append(str(result))
        return self.EvalPostArithmetic(Stack, Input, Index + 1)



    def is_float(self, s):
        try:
            float(s)
            return True
        except ValueError:
            return False


    def ResolveNumberVariable(self, Var):
        if Var.isnumeric() or self.is_float(Var):
            return Var
        elif Var in self.VarsNumber:
            return self.VarsNumber[Var]
        elif Var in self.VarsConst:
            return self.VarsConst[Var]
        self.Raise(1412, "Undefined variable or constant", "Scope Error")




    def IsObjectSet(self, Obj):
        for Key in Obj:
            if Obj[Key][0] == "[" and Obj[Key][len(Obj[Key])] == "]":
                return True
        return False



    def ExecuteAssignObject(self):
        Statement = self.Executable[self.POS]
        Obj = Statement.Right
        if self.IsObjectSet(Obj) or Statement.Left in self.VarsSet:
            self.GenerateFileSet(Obj, Statement.Left)
        else:
            self.TypeCheck(Statement.Left, self.VarsFile)
            File = FileObject()
            for Key in Obj:
                val = self.ResolveVarInSet(Obj[Key])
                setattr(File, str(Key).capitalize(), val)
            self.VarsFile[Statement.Left] = File




    def GenerateFileSet(self, Obj, Name):
        self.TypeCheck(Name, self.VarsSet)
        fileSet = SetObject()
        for Key in Obj:
            if hasattr(fileSet, Key.capitalize()):
                att = getattr(fileSet, Key.capitalize())
                if isinstance(Obj[Key], list):
                    for el in Obj[Key]:
                        att.append(self.ResolveVarInSet(el))
                else:
                    att.append(self.ResolveVarInSet(Obj[Key]))
                setattr(fileSet, Key.capitalize(), att)
            else:
                att = []
                if isinstance(Obj[Key], list):
                    for el in Obj[Key]:
                        att.append(self.ResolveVarInSet(el))
                else:
                    att.append(self.ResolveVarInSet(Obj[Key]))
                setattr(fileSet, Key, att)
        self.VarsSet[Name] = fileSet




    def ResolveVarInSet(self, Obj):
        if Obj[0] == "\"":
            return str(Obj).replace('"', '')
        elif str(Obj).isnumeric():
            return str(Obj)
        elif Obj in self.VarsString:
            return self.VarsString[Obj]
        elif Obj in self.VarsConst:
            return self.VarsConst[Obj]
        elif Obj in self.VarsNumber:
            return self.VarsNumber[Obj]
        else:
            self.Raise(1430, "Can't resolve property value", "Undefined Variable")



    def ExecuteAssignPath(self):
        Path = PathObject(self)
        Statement = self.Executable[self.POS]
        Path.Path = Statement.Right
        Path.Evaluate(Path.UnFoldAndCheck())
        self.VarsPath[Statement.Left] = Path




    def ExecuteStatement(self):
        Statement = self.Executable[self.POS]
        if Statement.Right[0] == "SELECT":
            self.ExecuteStatementSelect()
        elif Statement.Right[0] == "INSERT":
            self.ExecuteStatementInsert()
        elif Statement.Right[0] == "DELETE":
            self.ExecuteStatementDelete()




    def ExecuteStatementUpdate(self):
        Statement = self.Executable[self.POS]
        if Statement.Left != "INTO":
            self.Raise(1460, "Output statement need output Selector", "IO mismatch")
        self.TypeCheck(Statement.Selector, self.VarsPath)
        FileVar = Statement.Right[1]
        self.TypeCheck(FileVar, self.VarsFile)
        File = self.VarsFile[FileVar]
        FileVar = Statement.Right[2]
        self.TypeCheck(FileVar, self.VarsFile)
        newFile = self.VarsFile[FileVar]
        for pt in self.VarsPath[Statement.Selector].PossiblePaths:
            try:
                fullPath = os.path.join(pt, File.Name + File.Extension)
                with open(fullPath, 'w') as the_file:
                    the_file.write(newFile.Content)
                os.rename(fullPath, newFile.Name + newFile.Extension)
            except (FileExistsError, FileNotFoundError):
                self.Raise(1444, "Attempt to update non existing file", "Runtime Error")
            except PermissionError:
                self.Raise(1445, "Attempt to update file that you don't own", "Runtime Error")
            print("Update file '" + File.Name + File.Extension + "' in:" + pt)







    def ExecuteStatementDelete(self):
        Statement = self.Executable[self.POS]
        if Statement.Left != "FROM":
            self.Raise(1460, "Input statement need input Selector", "IO mismatch")
        self.TypeCheck(Statement.Selector, self.VarsPath)
        File = Statement.Right[1]
        self.TypeCheck(File, self.VarsFile)
        File = self.VarsFile[File]
        Paths = self.VarsPath[Statement.Selector]
        for path in Paths.PossiblePaths:
            try:
                os.remove(os.path.join(path, File.Name + File.Extension))
            except (FileExistsError, FileNotFoundError):
                self.Raise(1440, "Attempt to delete non existing file", "Runtime Error")
            except PermissionError:
                self.Raise(1442, "Attempt to delete file that you don't own", "Runtime Error")
            print("File '" + File.Name + File.Extension + "' deleted in " + path)


    def ExecuteStatementInsert(self):
        Statement = self.Executable[self.POS]
        if Statement.Left != "INTO":
            self.Raise(1460, "Output statement need output Selector", "IO mismatch")
        self.TypeCheck(Statement.Selector, self.VarsPath)
        FileVar = Statement.Right[1]
        File = self.VarsFile[FileVar]
        if File.Name == "":
            fname = ''.join(random.choice(string.ascii_lowercase + string.digits) for _ in range(10))
        else:
            fname = File.Name
        if File.Mime != "" and File.Content != "":
            mime = magic.from_buffer(File.Content, mime=True)
            if re.match(File.Mime, mime) is None:
                self.Raise(1432, "Attempt to write content of different format of provided mime")
        for pt in self.VarsPath[Statement.Selector].PossiblePaths:
            fullPath = os.path.join(pt, fname+File.Extension)
            with open(fullPath, 'w+') as the_file:
                the_file.write(File.Content)
            print("\nOne file written in: " + pt + "\n")




    def ExecuteStatementSelect(self):
        Statement = self.Executable[self.POS]
        if Statement.Left != "FROM":
            self.Raise(1460, "Input statement need input Selector", "IO mismatch")
        self.TypeCheck(Statement.Selector, self.VarsPath)
        titles = []
        conditions = ""
        i = 1
        Count = len(Statement.Right)
        datasetVar = None
        getTitles = True
        getConditions = False
        definedTitles = ["mime", "size", "name", "extension", "updated", "content", "path", "md5"]
        while i < Count:
            st = Statement.Right[i]
            if st in definedTitles and getTitles:
                titles.append(st)
            elif st == "," and getTitles:
                pass
            elif st == "WHERE":
                getConditions = True
                getTitles = False
            elif st == "it" and getConditions and Statement.Right[i+1] in ["==", "like"]:
                conditions = Statement.Right[i+2]
                self.TypeCheck(conditions, self.VarsSet)
                i += 2
            elif st == "->" and re.match('^(([a-z]|[A-Z]){1}\w*)|(_\w*)$', Statement.Right[i+1]) is not None:
                datasetVar = Statement.Right[i+1]
                self.TypeCheck(datasetVar, self.VarsDataset)
            i += 1
        paths = self.VarsPath[Statement.Selector]
        dtst = DatasetObject()
        dtst.Titles = titles
        if conditions == "":
            Set = SetObject()
        else:
            Set = self.VarsSet[conditions]
        for path in paths.PossiblePaths:
            for dirpath, dirs, files in os.walk(path):
                for file in files:
                    if self.IsFileInSet(os.path.join(dirpath, file), Set):
                        fl = FileObject()
                        fl = self.FileMeta(fl, os.path.join(dirpath, file))
                        dtst.Files.append(fl)
                break
        self.VarsDataset[datasetVar] = dtst




    def FileMeta(self, FlObj, File):
        FlObj.Mime = magic.from_file(File, mime=True)
        FlObj.Name = pathlib.Path(File).stem
        FlObj.Size = round(os.path.getsize(File) / 1024, 2)
        FlObj.Extension = pathlib.Path(File).suffix
        FlObj.Path = File
        if re.match(r"text\/.*", FlObj.Mime):
            with open(File, 'r') as content_file:
                FlObj.Content = content_file.read()    
        else:
            FlObj.Content = ""
        FlObj.Updated = os.path.getmtime(File)
        h = hashlib.md5()
        with open(File, 'rb') as content_file:
            h.update(content_file.read())
        FlObj.Md5 = h.hexdigest()
        return FlObj




    def IsFileInSet(self, File, Set):
        IsApplied = True
        if Set.Mime:
            mime = magic.from_file(File, mime=True)
            IsApplied = False
            res = re.match(r"(image\/jpeg)|(image\/png)|(image\/jpg)", mime)
            if Set.Path and res is not None and "similar" in  Set.Content:
                for reg in Set.Mime:
                    if re.match(reg, mime) is not None or \
                       re.match(r"(image\/jpeg)|(image\/png)|(image\/jpg)", mime):
                        filehash = imagehash.average_hash(Image.open(File))
                        for p in Set.Path:
                            pathhash = imagehash.average_hash(Image.open(p))
                            if abs(filehash - pathhash) < 10:
                                IsApplied = True
            else:
                for reg in Set.Mime:
                    if re.match(reg, mime) is not None:
                        IsApplied = True
                        break
        if IsApplied and Set.Content and re.match(r"text\/.*", mime):
            with open(File, 'r') as content_file:
                Content = content_file.read()
            IsApplied = False
            for reg in Set.Content:
                if re.search(reg, Content):
                    IsApplied = True
                    break
        if IsApplied and Set.Name:
            fname = pathlib.Path(File).stem
            IsApplied = False
            for reg in Set.Name:
                if re.match(reg, fname) is not None:
                    IsApplied = True
                    break
        if IsApplied and Set.Size and os.path.getsize(File) not in Set.Size:
            IsApplied = False
        if IsApplied and Set.Extension:
            ext = pathlib.Path(File).suffix
            IsApplied = False
            for reg in Set.Extension:
                if re.match(reg, ext) is not None:
                    IsApplied = True
                    break
        if IsApplied and Set.Updated and os.path.getmtime(File) not in Set.Updated:
            IsApplied = False
        if IsApplied:
            return True
        return False
