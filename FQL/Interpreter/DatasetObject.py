
class DatasetObject:

    def __init__(self):
        self.Files = []
        self.Titles = []


    def Print(self):
        MARGIN_SIZE = 33
        for title in self.Titles:
            print(self.DrawGrid(self.getMaxColWidth(title)), end='')
        print("+")
        print('|', end='')
        for title in self.Titles:
            margin = abs(self.getMaxColWidth(title) - len(title))
            remainder = margin % 2
            if remainder != 0:
                margin -= 1
            for i in range(0, margin//2):
                print(" ", end='')
            print(title, end='')
            for i in range(0, margin//2):
                print(" ", end='')
            if remainder != 0:
                print(" ", end='')
            print("|", end='')
        print('')
        for title in self.Titles:
            print(self.DrawGrid(self.getMaxColWidth(title)), end='')
        print('+')
        for file in self.Files:
            print('|', end='')
            for title in self.Titles:
                txt = str(getattr(file, str(title).capitalize()))
                if len(txt) > self.getMaxColWidth(title):
                    txt = txt[0:MARGIN_SIZE]
                margin = abs(self.getMaxColWidth(title) - len(txt))
                remainder = margin % 2
                if remainder != 0:
                    margin -= 1
                for i in range(0, margin//2):
                    print(" ", end='')
                print(txt, end='')
                for i in range(0, margin//2):
                    print(" ", end='')
                if remainder != 0:
                    print(" ", end='')
                print("|", end='')
            print('')
            for title in self.Titles:
                print(self.DrawGrid(self.getMaxColWidth(title)), end='')
            print('+')
        print("\n" + str(len(self.Files)) + " rows from set.\n")


    def getMaxColWidth(self, Col):
        MAX = 0
        for file in self.Files:
            tmp = len(str(getattr(file, str(Col).capitalize())))
            if tmp > MAX:
                MAX = tmp
        if MAX < 35:
            MAX += 8
        else:
            MAX = 48
        return MAX


    def DrawGrid(self, Size):
        GRID = "+"
        for i in range(0, Size):
            GRID += "-"
        return GRID
