from AST import AST

ROOT, LEFT, RIGHT = 0, 1, 2

class ASTAssign(AST):

    DataType = None

    def __init__(self, ExpectedType):
        self.DataType = ExpectedType
        super().__init__("Assign", 3)
      