from AST import AST

ROOT, LEFT, RIGHT = 0, 1, 2

class ASTElse(AST):

    def __init__(self, ExpectedType):
        self.DataType = ExpectedType
        super().__init__("Else", 3)
      