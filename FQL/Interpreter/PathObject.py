import sys
import os
import re
import pathlib
import magic
from PathElement import PathElement

class PathObject:

    StaticPath = ""
    Root = ""
    Path_Delimiter = "/"
    Path = []
    Interpreter = None


    def __init__(self, Interpreter):
        self.PossiblePaths = []
        if sys.platform.startswith("linux"):
            self.Root = "/"
            self.Path_Delimiter = "/"
        elif sys.platform.startswith("win32"):
            self.Root = os.environ['SYSTEMDRIVE']
            self.Path_Delimiter = "\\"
        self.Interpreter = Interpreter
        self.IsAcessedContain = False


    def UnFoldAndCheck(self):
        IsExecutable = False
        Path = PathElement(self.Path[0])
        Handler = Path
        Index = 1
        Count = len(self.Path)
        while Index < Count:
            element = self.Path[Index]
            if not IsExecutable and element != "{" and element != "}":
                Child = None
                Child = PathElement(element)
                Handler.SetChild(Child)
                Handler = None
                Handler = Child
            elif not IsExecutable and element == "{":
                IsExecutable = True
            elif IsExecutable and element == "}":
                IsExecutable = False
            elif IsExecutable and element != "{" and element != "}":
                if element == "it":
                    if self.Path[Index + 1] == "." and self.Path[Index + 2] in ["name"] and\
                       self.Path[Index + 3] in ["like"] and\
                       re.match('^(([a-z]|[A-Z]){1}\w*)|(_\w*)$', self.Path[Index + 4]) is not None:
                        ChildExec = None
                        ChildExec = PathElement(element, False)
                        for i in range(0, 5):
                            ChildExec.Element.append(self.Path[Index + i])
                        Handler.SetChild(ChildExec)
                        Handler = None
                        Handler = ChildExec
                        Index += 4
                elif element in ["and", "or"]:
                    ChildOp = None
                    ChildOp = PathElement(element, False)
                    Handler.SetChild(ChildOp)
                    Handler = None
                    Handler = ChildOp
                elif element == "contains":
                    if self.Path[Index + 1] == "only" and\
                    re.match('^(([a-z]|[A-Z]){1}\w*)|(_\w*)$', self.Path[Index + 2]) is not None:
                        ChildExec = None
                        ChildExec = PathElement(element, False)
                        for i in range(0, 3):
                            ChildExec.Element.append(self.Path[Index + i])
                        Handler.SetChild(ChildExec)
                        Handler = None
                        Handler = ChildExec
                        Index += 2
                    elif re.match('^(([a-z]|[A-Z]){1}\w*)|(_\w*)$', self.Path[Index + 1]) is not None:
                        ChildExec = None
                        ChildExec = PathElement(element, False)
                        for i in range(0, 3):
                            ChildExec.Element.append(self.Path[Index + i])
                        Handler.SetChild(ChildExec)
                        Handler = None
                        Handler = ChildExec
                        Index += 1
            Index += 1
        return Path


    def Evaluate(self, Statement):
        if Statement.Element == "Here":
            self.StaticPath = PathElement(os.getcwd())
        elif Statement.Element == "Disk":
            self.StaticPath = PathElement(self.Root)
        else:
            self.Interpreter.Raise(1450, "Undefined source", "Runtime Object Error")
        self.Crawler(Statement.Children[0])



    def Crawler(self, Statement):
        if Statement.IsStatic:
            self.StaticPath.Element = os.path.join(self.StaticPath.Element, Statement.Element)
            if Statement.Children:
                self.Crawler(Statement.Children[0])
        else:
            if Statement.Element[0] == "contains":
                self.CrawlerContains(Statement)
        if not self.IsAcessedContain and self.StaticPath.Element not in self.PossiblePaths:
            self.PossiblePaths.append(self.StaticPath.Element)




    def CrawlerContains(self, Statement):
        self.IsAcessedContain = True
        if Statement.Element[1] == "only":
            self.Interpreter.TypeCheck(Statement.Element[2], self.Interpreter.VarsSet)
            Set = self.Interpreter.VarsSet[Statement.Element[2]]
            setattr(Set, 'Only', True)
        else:
            self.Interpreter.TypeCheck(Statement.Element[1], self.Interpreter.VarsSet)
            Set = self.Interpreter.VarsSet[Statement.Element[1]]
            setattr(Set, 'Only', False)
        self.CrawlByCondition(Set, self.StaticPath.Element)



    def CrawlByCondition(self, Set, rootdir):
        AppliedPaths = []
        for subdir, dirs, files in os.walk(rootdir):
            IsApplied = True
            for file in files:
                IsApplied = True
                fullPath = os.path.join(subdir, file)
                if IsApplied and Set.Mime:
                    mime = magic.from_file(fullPath, mime=True)
                    IsApplied = False
                    for reg in Set.Mime:
                        if re.match(reg, mime) is not None:
                            IsApplied = True
                            break
                if IsApplied and Set.Name:
                    fname = pathlib.Path(file).stem
                    IsApplied = False
                    for reg in Set.Name:
                        if re.match(reg, fname) is not None:
                            IsApplied = True
                            break
                if IsApplied and Set.Size and os.path.getsize(fullPath) not in Set.Size:
                    IsApplied = False
                if IsApplied and Set.Extension:
                    ext = pathlib.Path(file).suffix
                    IsApplied = False
                    for reg in Set.Extension:
                        if re.match(reg, ext) is not None:
                            IsApplied = True
                            break
                if IsApplied and Set.Updated and os.path.getmtime(fullPath) not in Set.Updated:
                    IsApplied = False
                Only = getattr(Set, 'Only')
                if not IsApplied and Only:
                    break
                elif IsApplied and not Only:
                    break
            if IsApplied and files:
                AppliedPaths.append(subdir)

        #for Ap in AppliedPaths:
        self.PossiblePaths = AppliedPaths
