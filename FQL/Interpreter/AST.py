import sys
import os
sys.path.append(os.path.abspath(os.path.dirname(__file__) + '/')+"/Exceptions/")
from ASTException import ASTException

class AST:
    Type = ""
    ChildrenCount = 0
    Children = []
    Root = None
    Left = None
    Right = None
    Line = 0

    def __init__(self, Type, ChildrenCount):
        self.Type = Type
        self.ChildrenCount = ChildrenCount


    def Retrieve(self, Child):
        if(Child >= 0 and Child <= len(self.Children)):
            return self.Children[Child]
        else:
            pass
