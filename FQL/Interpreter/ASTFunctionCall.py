from AST import AST

ROOT, LEFT, RIGHT = 0, 1, 2

class ASTFunctionCall(AST):

    def __init__(self, ExpectedType):
        self.DataType = ExpectedType
        super().__init__("FunCall", 3)
      