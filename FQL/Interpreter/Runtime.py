
class Runtime:

    def __init__(self, Exec):
        self.Exec = Exec
        self.POS = 0

    def print(self, ArgList, Return=""):
        for Arg in ArgList:
            if Arg in self.Exec.VarsDataset:
                self.Exec.VarsDataset[Arg].Print()
            elif Arg in self.Exec.VarsPath:
                for pt in self.Exec.VarsPath[Arg]:
                    print(pt, end='')
            elif Arg in self.Exec.VarsNumber:
                print(str(self.Exec.VarsNumber[Arg]), end='')
            elif Arg in self.Exec.VarsString:
                print(self.Exec.VarsString[Arg], end='')
            elif Arg in self.Exec.VarsConst:
                print(self.Exec.VarsConst[Arg], end='')
            elif Arg[0] == '"' and Arg[len(Arg) - 1] == '"':
                print(str(Arg).replace('"', ''), end='')
            else:
                self.Exec.Raise(1422, "Can't resolve variable argument", "Runtime Error")
        print("\n")
        if Return != "":
            self.AssignReturn(Return, self.Exec.VarsString, "ok")




    def concat(self, ArgList, Return=""):
        txt = ""
        for Arg in ArgList:
            if Arg in self.Exec.VarsDataset:
                self.Exec.Raise(1405, "Can't stringfy Dataset", "Runtime Error")
            elif Arg in self.Exec.VarsPath:
                for pt in self.Exec.VarsPath[Arg]:
                    txt += str(pt)
            elif Arg in self.Exec.VarsNumber:
                txt += str(self.Exec.VarsNumber[Arg])
            elif Arg in self.Exec.VarsString:
                txt += str(self.Exec.VarsString[Arg])
            elif Arg in self.Exec.VarsConst:
                txt += str(self.Exec.VarsConst[Arg])
            else:
                txt += str(Arg).replace('"', '')
        if Return != "":
            self.AssignReturn(Return, self.Exec.VarsString, txt)



    def getcontent(self, ArgList, Return=""):
        self.Exec.TypeCheck(ArgList[0], self.Exec.VarsDataset)
        index = ArgList[1]
        dataset = self.Exec.VarsDataset[ArgList[0]]
        if len(dataset.Files) <= index:
            self.Exec.Raise(1451, "Index of dataset out of range", "Runtime Error")
        if Return != "":
            self.AssignReturn(Return, self.Exec.VarsString, dataset.Files[index].Content)


    def concatproperty(self, ArgList, Return=""):
        merged = ""
        self.Exec.TypeCheck(ArgList[0], self.Exec.VarsDataset)
        Dataset = self.Exec.VarsDataset[ArgList[0]]
        for file in Dataset.Files:
            if hasattr(file, str(ArgList[1]).capitalize()):
                merged += getattr(file, str(ArgList[1]).capitalize())
            else:
                self.Exec.Raise(1466, "Property undefined", "Runtime Error")
        if Return != "":
            self.AssignReturn(Return, self.Exec.VarsString, merged)




    def tostring(self, ArgList, Return=""):
        Arg = ArgList[0]
        if Arg in self.Exec.VarsDataset:
            self.Exec.Raise(1405, "Can't stringfy Dataset", "Runtime Error")
        elif Arg in self.Exec.VarsPath:
            for pt in self.Exec.VarsPath[Arg]:
                txt = str(pt)
        elif Arg in self.Exec.VarsNumber:
            txt = str(self.Exec.VarsNumber[Arg])
        elif Arg in self.Exec.VarsString:
            txt = str(self.Exec.VarsString[Arg])
        elif Arg in self.Exec.VarsConst:
            txt = str(self.Exec.VarsConst[Arg])
        elif Arg[0] == '"' and Arg[len(Arg) - 1] == '"':
            txt = str(Arg).replace('"', '')
        else:
            self.Exec.Raise(1422, "Can't resolve variable argument", "Runtime Error")
        if Return != "":
            self.AssignReturn(Return, self.Exec.VarsString, txt)




    def tonum(self, ArgList, Return=""):
        txt = 0
        Arg = ArgList[0]
        if Arg in self.Exec.VarsDataset:
            self.Exec.Raise(1407, "Can't numberify Dataset", "Runtime Error")
        elif Arg in self.Exec.VarsPath:
            for pt in self.Exec.VarsPath[Arg]:
                txt = self.Number(pt)
        elif Arg in self.Exec.VarsNumber:
            txt = self.Exec.VarsNumber[Arg]
        elif Arg in self.Exec.VarsString:
            txt = self.Number(self.Exec.VarsString[Arg])
        elif Arg in self.Exec.VarsConst:
            txt = self.Number(self.Exec.VarsConst[Arg])
        elif Arg[0] == '"' and Arg[len(Arg) - 1] == '"':
            txt = self.Number(str(Arg).replace('"', ''))
        else:
            self.Exec.Raise(1422, "Can't resolve variable argument", "Runtime Error")
        if Return != "":
            self.AssignReturn(Return, self.Exec.VarsString, txt)


    def Number(self, val):
        fltval = float(val)
        intval = int(val)
        if fltval == intval:
            return intval
        return fltval




    def count(self, ArgList, Return=""):
        if ArgList[0] in self.Exec.VarsDataset:
            val = len(self.Exec.VarsDataset[ArgList[0]].Files)
        elif ArgList[0] in self.Exec.VarsPath:
            val = len(self.Exec.VarsPath[ArgList[0]].PossiblePaths)
        if Return != "":
            self.AssignReturn(Return, self.Exec.VarsNumber, val)


    def tocsv(self, ArgList, Return=""):
        dt = ArgList[0]
        if dt not in self.Exec.VarsDataset:
            self.Exec.Raise(1430, "Expected dataset object", "Runtime Error")
        dataset = self.Exec.VarsDataset[dt]
        csv = ""
        for title in dataset.Titles:
            csv += str(title) + ", "
        csv = csv.strip()
        if csv[len(csv) - 1] == ",":
            csv = csv[:-1]
        csv += "\n"
        for file in dataset.Files:
            for title in dataset.Titles:
                csv += str(getattr(file, str(title).capitalize())) + ", "
            csv = csv.strip()
            if csv[len(csv) - 1] == ",":
                csv = csv[:-1]
            csv += "\n"
        if Return != "":
            self.AssignReturn(Return, self.Exec.VarsString, csv)




    def AssignReturn(self, Var, Vars, Val):
        self.Exec.TypeCheck(Var, Vars)
        Vars[Var] = Val
