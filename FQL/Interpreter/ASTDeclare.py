from AST import AST

ROOT, LEFT, RIGHT = 0, 1, 2

class ASTDeclare(AST):

    DataType = ""

    def __init__(self, DT):
        self.DataType = DT
        super().__init__("Declare", 3)  
        