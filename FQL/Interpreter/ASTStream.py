from AST import AST
from ASTException import ASTException

class ASTStream:
    Statements = []
    Type = ""


    def Append(self, item):
        if not isinstance(item, AST):
            AE = ASTException(1000, "Non AST node in ASTStream",
                              "ASTStream@Append", "Internal Error")
            AE.Raise()
        self.Statements.append(item)


    def Insert(self, index, item):
        if not isinstance(item, AST):
            AE = ASTException(1000, "Non AST node in ASTStream",
                              "ASTStream@Insert", "Internal Error")
            AE.Raise()
        elif(index < 0 or index > len(self.Statements)):
            AE = ASTException(1001, "Index out of Range ", "ASTStream@Insert", "Internal Error")
            AE.Raise()
        self.Statements.insert(index, AST)
    