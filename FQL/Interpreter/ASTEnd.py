from AST import AST

ROOT, LEFT, RIGHT = 0, 1, 2

class ASTEnd(AST):

    End = ""

    def __init__(self, ExpectedType):
        self.DataType = ExpectedType
        self.End = ExpectedType
        super().__init__("End", 3)
      