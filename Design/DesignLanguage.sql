FROM Disk: SELECT meta WHERE mime = TEXT_FILE AND content = '/^log x$/'
FROM Index: SELECT 
INTO Disk: INSERT Content as File {name: 'name', permissions:XWR, sizeMax:420K}

DATA TYPES: 
- File
- Path
- Content
- Number
- TEXT
- Meta Map
- Event
- File SET
- lists


stdlib.fql
{
    mime = {text: foij, hipopr}
}

path p = Here:/core/silk:*:Q --Here is dynamic path, : is operator.
path v = Virtual:Sets/music/ 
/**
*Path data type
 Source:Path_in_Source
 Path support dynamic linking so you can write Source:path/*.*
 Path define set of 'selectors' to determine search path in dierctory such as
 Source:path/{mime=text} this is search path that will link to all files having mime 'text'
 Path supports tree traversing technique since logically all directories are -effectively- search trees
 Path can be seen as regex matcher for files, you can determine pattern according to file/dir name but also according 
 to file meta data or even content, path will return tree that represents all the possible paths in directory given the 
 conditions.
 */
FROM v: SELECT content, name---------------------------------------------------------- WHERE Belongs To Set ;

file stream = {name:'meth', mime: 'text', content:@acc}
INTO v: INSERT Stream 
/**
*FROM keyword
 FROM path: do_something
 From is selector keyword or input stream, it determine the source of data set that the next 
 statement will execute against.
 
 *SELECT Keyword
 is similar to sql select keyword. It return needed data from the specific selector given they apply the 
 conditions. 'name'. 

 *INSERT keyword
 is similar to INSERT in SQL, it write file object in specified selector 

 *INTO keyword
 Similar to INTO in SQL, it is the inverse of FROM, it is the output stream(or output Selector)

 *Variable declaration
 we use C style variables. datatype variable;

 *Constant set 
 use keyword const constant_name = value;

 *function declaration 
 use keyword function. 
 function function_name(arguments)
  statements
 end function

 *Control flow
  if(condition)
    statements
  else if(condition)
    statements
  else
    final
  end if

  *Loop
    while(condition)
        statements
    end while

  *return keyword 
   used at the end of function to return value.

  *Standard Runtime::Cload()

  *use stdlib; 

  *operators:
  +, -, /, *, %, :, ==, >, <, !, >=, <=
    
*/

FROM Disk: Index 

function C()


end function 